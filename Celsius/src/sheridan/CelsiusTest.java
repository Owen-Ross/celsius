/**
 * @author Owen Ross
 */
package sheridan;

import static org.junit.Assert.*;

import org.junit.Test;

public class CelsiusTest {

	@Test
	public void testCelsiusRegular() {
		int tempurature = Celsius.fromFahrenheit(65);
		System.out.println(tempurature);
		assertTrue("Not a valid tempurature", tempurature == 18);
	}
	
	@Test
	public void testCelsiusException() {
		int tempurature = Celsius.fromFahrenheit(-45);
		System.out.println(tempurature);
		assertFalse("Not a valid tempurature", tempurature == -43);
	}

	
	@Test
	public void testCelsiusBoundrayIn() {
		double input = (Math.round(45.6));
		int tempurature = Celsius.fromFahrenheit((int)input);
		System.out.println(tempurature);
		assertTrue("Not a valid tempurature", tempurature == 7);
	}
	
	@Test
	public void testCelsiusBoundrayOut() {
		int tempurature = Celsius.fromFahrenheit((int)(Math.round(45.4)));
		System.out.println(tempurature);
		assertFalse("Not a valid tempurature", tempurature == 8);
	}
}
